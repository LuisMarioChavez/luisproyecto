#include <stdio.h>
#include <conio.h>
#include <ctype.h>

void main (void)
{
 /*declaraciones*/
 float fSueldo, fHoras, fHorasExtras, fCompensacion;
 char cContinua;
 char szNombre[80];
 
 
/*parametros de sistema*/
const float MAX_HORAS =50;
const float SAL_NORMAL =11000;
const float FAC_EXTRAS =1.5;
const int LIM_EXTRAS =30;

printf("\n\t\t Calculo de la nomina");
printf ("\n\t\t ====================\n\n");

/*control del programa*/
cContinua = 'S';
while (cContinua != 'N')
{

/*lectura nombre del empleado*/
printf("\n Nombre del empleado: ");
szNombre[0]='\0';
while ( szNombre [0]=='\0')
{
flushall;
gets (szNombre);
if (szNombre[0]=='0')
   printf("\n Digite el nombre del empleado\n");
}


/*lectura de horas trabajadas*/
fHoras =0.0;
while (fHoras<=0 || fHoras>MAX_HORAS)
{
 printf ("\n Digite el numero de horas trabajadas: ");
 scanf ("%f",&fHoras);
 if (fHoras<=0)
    printf ("\n El numero de horas debe ser positivo ");
	else 
	if (fHoras>MAX_HORAS)
	printf ("\n El maximo numero de horas es %5.1f",MAX_HORAS);
}

/*calculo de salario */
fSueldo =SAL_NORMAL;
fHorasExtras = 0;
if (fHoras<=LIM_EXTRAS)
{
	
  fSueldo = SAL_NORMAL*fHoras;
}
else{
	
	fHorasExtras= fHoras-LIM_EXTRAS;
	fCompensacion= fHorasExtras*SAL_NORMAL*FAC_EXTRAS;
	fSueldo= SAL_NORMAL*LIM_EXTRAS;
	fSueldo= fSueldo+fCompensacion;
}

/*despliegue de resultados*/
printf("\n\n Salario correspondiente a %s: ",szNombre);
if  (fHoras<=LIM_EXTRAS)
{
	printf ("\n\n\t Sueldo: %9.2f",fSueldo);
}
else
{
	printf("\n\n\t Sueldo Normal: %9.2f",SAL_NORMAL*LIM_EXTRAS);
	printf("\n\t Horas extras: %9.2f",fHorasExtras);
	printf ("\n\t Compensacion: %9.2f",fCompensacion);
	printf("\n\t ==========");
	printf("\n\t Total a pagar: %9.2f",fSueldo);
}
    printf("\n\n Desea otro calculo?[S/N]");
    cContinua=toupper(getche());
}
	
	printf ("\n");
}
	
	
	
	

	
	
	
	
	
		
	
	
	
	
	
	
	
	
	
	

